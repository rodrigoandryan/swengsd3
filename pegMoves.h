#ifndef boardSizeX
#define boardSizeX 5
#endif
#ifndef boardSizeY
#define boardSizeY 5
#endif

#ifndef pegMoves
#define pegMoves
bool validMovesLeft(bool board[boardSizeX][boardSizeY]) {
	for(int j=0; j<boardSizeY; j++) {
		for(int i=0; i<boardSizeX; i++) {
			if(board[i][j]==1) { //can only have valid moves if it is a peg!
				if(i<2) { //Check moves only to the right
					if(board[i+1][j]==1 && board[i+2][j]==0) //Peg and hole to the right
						return true;
				}
				else if(i>=(boardSizeX-2)) { //Check moves only to the left
					if(board[i-1][j]==1 && board[i-2][j]==0) //Peg and hole to the left
						return true;
				}
				else {
					if(board[i+1][j]==1 && board[i+2][j]==0) //Peg and hole to the right
						return true;
					if(board[i-1][j]==1 && board[i-2][j]==0) //Peg and hole to the left
						return true;
				}

				if(j<2) { //Check moves only to the bottom
					if(board[i][j+1]==1 && board[i][j+2]==0) //Peg and hole below
						return true;
				}
				else if(j>=(boardSizeY-2)) { //Check moves only to the top
					if(board[i][j-1]==1 && board[i][j-2]==0) //Peg and hole to the left
						return true;
				}
				else {
					if(board[i][j+1]==1 && board[i][j+2]==0) //Peg and hole below
						return true;
					if(board[i][j-1]==1 && board[i][j-2]==0) //Peg and hole above
						return true;
				}
			}
		}
	}
	return false; //If it makes it to the bottom, there are no more possible moves
}

bool validMovesPeg(int xval, int yval, bool board[boardSizeX][boardSizeY], int* error) { //Check chosen peg for valid moves
	*error=0;
	if(board[xval][yval]==0) { //Chosen peg is a hole!
		*error=3;
		return false;
	}
	if(xval<2) { //Check moves only to the right
		if(board[xval+1][yval]==1 && board[xval+2][yval]==0) { //Peg and hole to the right
			return true;
		}
	}
	else if(xval>(boardSizeX-2)) { //Check moves only to the left
		if(board[xval-1][yval]==1 && board[xval-2][yval]==0) { //Peg and hole to the left
			return true;
		}
	}
	else {
		if(board[xval+1][yval]==1 && board[xval+2][yval]==0) { //Peg and hole to the right
			return true;
		}
		if(board[xval-1][yval]==1 && board[xval-2][yval]==0) { //Peg and hole to the left
			return true;
		}
	}

	if(yval<2) { //Check moves only to the bottom
		if(board[xval][yval+1]==1 && board[xval][yval+2]==0) { //Peg and hole below
			return true;
		}
	}
	else if(yval>(boardSizeY-2)) { //Check moves only to the top
		if(board[xval][yval-1]==1 && board[xval][yval-2]==0) { //Peg and hole to the left
			return true;
		}
	}
	else {
		if(board[xval][yval+1]==1 && board[xval][yval+2]==0) { //Peg and hole below
			return true;
		}
		if(board[xval][yval-1]==1 && board[xval][yval-2]==0) { //Peg and hole above
			return true;
		}
	}
	*error=3; //Invalid move, chosen peg has no moves
	return false; //Chosen peg has no valid moves
}

int validMove(int xval, int yval, int xtar, int ytar, bool board[boardSizeX][boardSizeY]) { //Check chosen peg for valid moves
	//As the player input function checks for appropriate distances,
	//this function only checks for holes and pegs in the proper places
	int xDir = xtar - xval; //Gets the x direction of the target
	int yDir = ytar - yval; //Gets the y direction of the target
	//As the target direction can only be in either the x or y direction,
	//when the target and current value are the same, the direction in that plane will be zero 
	if(xval < 2 && xDir<0) //Near the left edge, moving left
		return 2; //Invalid move, off the edge of the board
	if(xval > 2 && xDir>0) //Near the right edge, moving right
		return 2; //Invalid move, off the edge of the board
	if(yval < 2 && yDir<0) //Near the top edge, moving up
		return 2; //Invalid move, off the edge of the board
	if(yval > 2 && yDir>0) //Near the bottom edge, moving down
		return 2; //Invalid move, off the edge of the board

	if(board[xval+xDir][yval+yDir]==0 && //Target is a hole
	board[xval+xDir/2][yval+yDir/2]==1) //Target jumps over a peg
		return 0; //Valid move
	if(board[xtar][ytar]==1)
		return 1; //Invalid move, cannot move to an occupied spot
	if(board[xval+xDir/2][yval+yDir/2]==0)
		return 4; //Invalid move, cannot jump over a hole
	if(xtar<1 || xtar>boardSizeX || ytar<1 || ytar>boardSizeY)
		return 2; //Invalid move, off the edge of the board
}

void movePeg(int *xcoord, int *ycoord, int xtar, int ytar, bool board[boardSizeX][boardSizeY]) {
	board[*xcoord][*ycoord]=0; //Change chosen peg to a hole
	board[xtar][ytar]=1; //Change target hole to a peg
	board[*xcoord+(xtar-*xcoord)/2][*ycoord+(ytar-*ycoord)/2]=0; //Change jumped peg to a hole
	*xcoord=xtar;*ycoord=ytar; //Update the current peg, only matters in 2 player
	return;
}
#endif
