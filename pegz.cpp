#include "UI.h"
#include"pegMoves.h"
#include<time.h>

using namespace std;

int main()
{ 
	bool board[boardSizeX][boardSizeY]; //The board is an array of booleans
						//A one is a peg, and a zero is a hole
	bool keepPlaying=true;

	while(keepPlaying) { //Start the game, and restart it if so chosen
		for(int i=0; i<boardSizeX; i++) { //Fill the entire board with pegs
			for(int j=0; j<boardSizeY; j++) {
				board[i][j] = 1;
			}
		}
		srand(time(NULL));
		board[rand()%boardSizeX][rand()%boardSizeY] = 0; //Remove a single peg from a random slot

		int xval=0, yval=0, xtar=0, ytar=0, player1 = 0, player2 = -1; 
		bool player = 0; //false (zero) is player 1, true (one) is player 2
			//Defaults to player one
		/*
		variables for player input function
		xval and yval are the peg that will be moved
		xtar and ytar are the coords of the location the peg will land
		player is the player number
		for one player it should always be given as 1
		for two player the calling function should give it 1 or 2 
		depending on whose turn it is
		player1 and player2 are each players scores.
		for 1 player print mode, player2 argument should be -1
		if player2 != -1, it will print both scores
		*/


		printGameboard(); //Set the default display

		int numplayers = numPlayers(); //get the number of players
		int errorCode = 0;

		printGameboard(); //reset game board incase they entered really long invalid input

		updateGameboard(board);

		bool validPeg;

		while(validMovesLeft(board)) { //Loop until there are no moves left
			do{
				playerInput(xval, yval, xtar, ytar, player?2:1); //Prompt for player input
				validPeg = validMovesPeg(xval, yval, board, &errorCode);
				badMove(errorCode);
				errorCode = 0; //Reset errorCode
			} while(!validPeg); //repeat until a valid peg is selected

			while(badMove(validMove(xval,yval,xtar,ytar,board))) //Verify the move is valid
				playerInput(xval, yval, xtar, ytar, player?2:1); //Reprompt for player input if it is not

			movePeg(&xval, &yval, xtar, ytar, board); //Move the peg
			updateGameboard(board);

			if(numplayers==2) { //Only for 2 players,
				while(validMovesPeg(xval, yval, board, &errorCode)) { //Continue moving current peg until there are no more moves
					badMove(errorCode);
					errorCode = 0; //Reset errorCode
					playerJump(xtar, ytar, xval, yval, player?2:1); //Prompt for chosen peg's next target
					while(badMove(validMove(xval,yval,xtar,ytar,board))) //Verify the move is valid
						playerJump(xtar, ytar, xval, yval, player?2:1); //Reprompt for player input if it is not
					movePeg(&xval, &yval, xtar, ytar, board); //Move the peg
					player?player2++:player1++; //increment score for current player
					updateGameboard(board);
				}
			}

			player?player2++:player1++; //increment score for current player

			if(numplayers == 2) { //Toggle player
				player = !player;
			}
		}
		keepPlaying=endGame(player1, player2);
	}


	gotoxy(25,20);
	//cout << xval << " " << yval << " " << xtar << " " << ytar;

	system("pause");
	return 0;
}
