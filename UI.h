/*
Made By: Ryan Dean and Rodrigo Sanchez-Vicarte

This file includes all of the UI needed for the PEGZ game program.
It requires consoleMove.cpp to function properly.

revision history:
3/11: created majority of gameboard function
3/12: finished gameboard function, created numplayer function, created updateGameboard function, started playerInput function
3/13: finished player input function, made badmove function, made printscore function, started endGame function
3/14: finished endGame function, updated badMove function with specific errors
3/22: fixed previous entered move not being cleared
*/

#ifndef UI
#define UI
#include <string>
#include <iostream>
#include<Windows.h>
using namespace std;

/*
Created: 11/18/04
Modified/Header Author: Jason Olsen
Original code from:
"http://www.codeproject.com/cpp/cppforumfaq.asp?msg=729023#cons_gotoxy"
*/
void gotoxy(int x, int y)
{
	HANDLE hConsole = GetStdHandle ( STD_OUTPUT_HANDLE );

    if ( INVALID_HANDLE_VALUE != hConsole )
        {
        COORD pos = {x, y};
        SetConsoleCursorPosition ( hConsole, pos );
        }
    return;
}

/*
Credit for resizing the window code:
Author: Dustin Boyd / rpgfan3233 (forum name)
Created: April 20, 2008
Original code from: http://www.cplusplus.com/forum/beginner/1481/
*/
void resizexy(int x, int y)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //stores the console's current dimensions
	//MoveWindow(window_handle, x, y, width, height, redraw_window);
	MoveWindow(console, r.left, r.top, x, y, TRUE);
}

/*
This function prints the gameboard with some instructions.
If you want to change the instructions just let me know what you want them to say.
I would advise calling this function before and after calling numPlayers, incase any invalid input was entered.
*/
void printGameboard()
{
	resizexy(700,350);
	gotoxy(0,0);
	cout << "------------------------------- Welcome to PEGZ! ------------------------------\n"
		 << "|                                                                             |\n"
		 << "|  Game Board:           Instructions: The goal is to remove as many pegs as  |\n"
		 << "|                                      possible.  To remove a peg, you must   |\n"
	     << "|     A  B  C  D  E                    jump it with another peg.  The used    |\n"
		 << "|                                      peg must have an space to land in.     |\n"
		 << "|  1  O  O  O  O  O                    The jumped peg is then removed.        |\n"
		 << "|                                                                             |\n"
		 << "|  2  O  O  O  O  O                    The format for entering a move is:     |\n"
		 << "|                                      PegColumn PegRow MoveDirection         |\n"
		 << "|  3  O  O  O  O  O                    Example: A1D                           |\n"
		 << "|                                                                             |\n"
		 << "|  4  O  O  O  O  O                    Direction Table:                       |\n"
		 << "|                                      U = Up      D = Down                   |\n" 
		 << "|  5  O  O  O  O  O                    L = Left    R = Right                  |\n"
		 << "|                                                                             |\n"
		 << "|                                                                             |\n"  
		 << "|                                                                             |\n" 
		 << "|                                                                             |\n" 
		 << "|                                                                             |\n" 
		 << "|                                                                             |\n" 
		 << "-------------------------------------------------------------------------------\n"
		 <<endl;
	return;
}

/*
This function asks the user for the number of players.
It ignores any invalid input and does its best to clean up the invalid input.
It returns the number of players as an integer.
*/
int numPlayers()
{
	int num=0; //num to hold number of players

	//Print out instructions to enter number of players and then read in num of players
	gotoxy(25,16);
	cout << "Would you like 1 player game or 2 player game?";
	gotoxy(25,17);
	cout << "Enter 1 for 1 player or enter 2 for 2 player.";
	gotoxy(25,18);
	cin >> num;

	//If invalid input was entered, remove it and ask for player num again
	while(num != 1 && num != 2)
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n'); //clean up invalid input
		
		gotoxy(25,16);
		cout << "Invalid input entered.  Try again.            "; //This overwrites the enter player question
		
		gotoxy(25,18);
		cout << "                                              "; //This line clears the invalid input that the user entered
		
		gotoxy(25,18);
		cin >> num;
	}

	return num;
}

/*
This function prints out (updates) the gameboard.  
That is all it does.
For gotoxy commands:
Columns(x) = 6 9 12 15 18
Rows(y) = 6 8 10 12 14
3i+6 for x
2j+6 for y
*/
void updateGameboard(bool board[5][5])
{
	int i = 0, j = 0, x = 0, y = 0; //local variables

	for(i = 0; i < 5; i++)
		for(j = 0; j < 5; j++) //two loops since it is a 2d array
		{
			x = (3 * i) + 6; //find correct x position on console window
			y = (2 * j) + 6; //find correct y position on console window

			gotoxy(x,y); //goto the position to print out that peg

			if(board[i][j] == TRUE)	//if there is a peg in the array print a peg
				cout << "O";
			else
				cout << " "; //else just print a blank
		}
	return;
}

/*
This function ask the user to enter a peg move.
If the format they entered is incorrect it will ask them to try again.
It does not check whether the move is valid or not.
It returns xval, yval, xtar, and ytar by reference.
varaibles for player input function:
xval and yval are the peg that will be moved
xtar and ytar are the coords of the location the peg will land
these are the array index so it will return 0 0 0 2 if A1R was entered
player is the player number
for one player it should always be given as 1
for two player the calling function should give it 1 or 2 
depending on whose turn it is
*/
void playerInput(int &xval, int &yval, int &xtar, int &ytar, int player)
{
	string temp;
	
	gotoxy(25,17);
	cout << "Player " << player << ": Please enter a valid move."; //Ask use to enter a valid move

	cin.clear();
	gotoxy(25,18);
	//cin >> temp;
	getline(cin, temp);
	
	while(((temp[0] != 'A' && temp[0] != 'B' && temp[0] != 'C' && temp[0] != 'D' && temp[0] != 'E') || //this covers all valid input strings
		  (temp[1] != '1' && temp[1] != '2' && temp[1] != '3' && temp[1] != '4' && temp[1] != '5') ||
		  (temp[2] != 'U' && temp[2] != 'D' && temp[2] != 'L' && temp[2] != 'R')) || temp == "")
	{
		temp.clear();
		cin.clear();
		cin.ignore(INT_MAX, '\n'); //clean up invalid input

		gotoxy(25,16);
		cout << "Invalid input entered.  Try again.            "; //Alert the user that they entered an invalid input.
		
		gotoxy(25,18);
		cout << "                                              "; //This line clears the invalid input that the user entered
		
		gotoxy(25,18); //ask user to try again
		//cin >> temp;
		getline(cin, temp);
	}
	
	gotoxy(25,16);
	cout << "                                                  "; //clearing old messages
	gotoxy(25, 18);
	cout << "                                              ";//clearing old input
	//This section will decode the player input string into the 4 peg variables

	xval = ((int)temp[0]-65); //convert x position letter to array index x
	yval = ((int)temp[1]-49); //convert y position number to array index y

	if(temp[2] == 'U') //depending on the direction of the move, convert the xtar and ytar accordingly
	{
		xtar = xval;
		ytar = yval - 2;
	}
	else if(temp[2] == 'D')
	{
		xtar = xval;
		ytar = yval + 2;
	}
	else if(temp[2] == 'L')
	{
		xtar = xval - 2;
		ytar = yval;
	}
	else if(temp[2] == 'R')
	{
		xtar = xval + 2;
		ytar = yval;
	}
	return;
}

/*
This function asks the player to choose the direction for the next jump they wish to make.
It must be sent the starting peg position (xval, yval) so that it can determine
the final coordinates of the jump.
*/
void playerJump(int &xtar, int &ytar, int xval, int yval, int player)
{
	string temp;

	gotoxy(25, 17);
	cout << "Player " << player << ": Please enter a jump direction."; //Ask use to enter a valid jump direction

	cin.clear();
	gotoxy(25, 18);
	getline(cin, temp);

	while (temp[0] != 'U' && temp[0] != 'D' && temp[0] != 'L' && temp[0] != 'R')
	{
		temp.clear();
		cin.clear();
		cin.ignore(INT_MAX, '\n'); //clean up invalid input

		gotoxy(25, 16);
		cout << "Invalid input entered.  Try again.            "; //Alert the user that they entered an invalid input.

		gotoxy(25, 18);
		cout << "                                              "; //This line clears the invalid input that the user entered

		gotoxy(25, 18); //ask user to try again
		getline(cin, temp);
	}

	gotoxy(25, 16);
	cout << "                                                  "; //clearing old messages
	gotoxy(25, 17);
	cout << "                                                  "; //clearing old messages
	gotoxy(25, 18);
	cout << "                                                  "; //clearing old messages

	if (temp[0] == 'U') //depending on the direction of the move, convert the xtar and ytar accordingly
	{
		ytar = yval - 2;
		xtar = xval;
	}
	else if (temp[0] == 'D')
	{
		ytar = yval + 2;
		xtar = xval;
	}
	else if (temp[0] == 'L')
	{
		xtar = xval - 2;
		ytar = yval;
	}
	else if (temp[0] == 'R')
	{
		xtar = xval + 2;
		ytar = yval;
	}
	return;
}
/*
This function should be called once your function determines that a move was not allowed.
It will tell the user that the entered move was not allowed.
You should call playerInput after badMove to get the player input again.
1. Move to occupied spot 2. move off the edge 3. choosen peg can make no moves, 4. attempt to jump over hole. 
*/
bool badMove(int errorCode)
{
	if (errorCode == 0) //no error
		return false; //Return false, do not continue looping for input
	if(errorCode == 1)
	{
		gotoxy(25,16); 
		cout << "Can not move peg to an occupied spot.  Try again."; //read error code then print message
	}

	else if(errorCode == 2)
	{
		gotoxy(25,16); 
		cout << "Can not move peg off the board.  Try again.      ";
	}

	else if(errorCode == 3)
	{
		gotoxy(25,16); 
		cout << "Selected peg has no valid moves.  Try again.     ";
	}

	else if(errorCode == 4)
	{
		gotoxy(25,16); 
		cout << "Can not jump a peg over a hole.  Try again.      ";
	}

	gotoxy(25,18);
	cout << "                                              "; //This line clears the invalid input that the user entered

	return true; //Return true, continue looping for input
}

/*
This function prints the player score, need to send it both players score.
To print for 1 player mode, player 2 score should be -1.
For 2 player print mode, if player 2 score is not -1, it will print both players scores.
I did it this way so you don't have to pass it as many variables.
*/
void printScore(int player1, int player2)
{
	gotoxy(3,16);
	cout << "Player 1 Score: " << player1;

	if(player2 != -1)
	{
		gotoxy(3,17);
		cout << "Player 2 Score: " << player2;
	}
	return;
}

/*
This function ends the game.
It prints out the winner or tie then asks if they want to play again.
It returns true if they want to play again, otherwise it returns false.
*/

bool endGame(int player1, int player2)
{
	char temp;

	gotoxy(25,16);
	cout << "                                              ";//cleaning up old inputs and messages
	gotoxy(25,17);
	cout << "                                              ";
	gotoxy(25,18);
	cout << "                                              ";

	gotoxy(3,16);
	cout << "Player 1 Score: " << player1; //printing final scores

	if(player2 != -1)
	{
		gotoxy(3,17);
		cout << "Player 2 Score: " << player2;
		gotoxy(25,16);

		if(player1 == player2)
			cout << "Tie Game!                                 "; //tie or win game for each player
		
		else if(player1 > player2)
			cout << "Player 1 Wins!                            ";

		else
			cout << "Player 2 Wins!                            ";
	}

	else
	{
		gotoxy(25,16);
		cout << "Good Job Player 1!                        "; //single player score message
	}

	//This section will ask the user if they want to play again.

	gotoxy(25,17);
	cout << "Would you like to play again? Y for yes or N for no.";

	gotoxy(25,18);
	cin >> temp;

	//If invalid input was entered, remove it and ask for input again
	while(temp != 'Y' && temp != 'N')
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n'); //clean up invalid input
		
		gotoxy(25,16);
		cout << "Invalid input entered.  Try again.            "; //This overwrites the enter player question
		
		gotoxy(25,18);
		cout << "                                              "; //This line clears the invalid input that the user entered
		
		gotoxy(25,18);
		cin >> temp;
	}

	if(temp == 'Y')
		return TRUE; //return true if player wants to play again
	else
		return FALSE;
}
#endif
